package com.logistics.micro.common.model;

import com.logistics.micro.common.enums.HeaderStatus;
import lombok.Data;

/**
 * 公共返回类头部信息
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-25 21:50
 */
@Data
public class Header {
    private Integer code;
    private String msg;

    public Header(HeaderStatus headerStatus) {
        this.code = headerStatus.getCode();
        this.msg = headerStatus.getMsg();
    }
}
