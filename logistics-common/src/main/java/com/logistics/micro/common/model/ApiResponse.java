package com.logistics.micro.common.model;

import com.google.gson.Gson;
import com.logistics.micro.common.enums.HeaderStatus;
import lombok.Data;
import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * 控制器全局返回对象
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-25 21:49
 */
@Data
public class ApiResponse<T> {
    private Header header;
    private T body;

    public ApiResponse(HeaderStatus headerStatus) {
        this.header = new Header(headerStatus);
    }

    public ApiResponse(T body) {
        this.header = new Header(HeaderStatus.SUCCESS);
        this.body = body;
    }

    public ApiResponse(HeaderStatus headerStatus, T body) {
        this.header = new Header(headerStatus);
        this.body = body;
    }

    public ApiResponse() {
        this.header = new Header(HeaderStatus.ERROR);
    }

}
