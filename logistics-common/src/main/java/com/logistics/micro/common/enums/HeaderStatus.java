package com.logistics.micro.common.enums;

/**
 * 返回头枚举类型，用于进行返回引用
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-25 21:53
 */
public enum HeaderStatus {

    SUCCESS(200, "ok"),
    ERROR(500, "error"),
    PERMISSION_DIED(501, "您没有权限进行访问，请联系管理员"),
    ;


    private final Integer code;
    private final String msg;

    HeaderStatus(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
