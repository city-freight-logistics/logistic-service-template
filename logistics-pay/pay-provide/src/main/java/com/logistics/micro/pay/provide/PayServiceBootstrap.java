package com.logistics.micro.pay.provide;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-24 22:21
 */
@SpringBootApplication
@MapperScan("com.logistics.micro.pay.provide.mapper")
public class PayServiceBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(PayServiceBootstrap.class, args);
    }
}
