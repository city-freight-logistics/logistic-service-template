package com.logistics.micro.pay.provide.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logistics.micro.pay.provide.model.pojo.ExPojo;

/**
 * 示例Mapper
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-3-23 22:32
 */
public interface ExpMapper extends BaseMapper<ExPojo> {
}
