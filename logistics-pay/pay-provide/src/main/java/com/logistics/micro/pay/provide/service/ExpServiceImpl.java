package com.logistics.micro.pay.provide.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logistics.micro.pay.api.ExpPayApiService;
import com.logistics.micro.pay.provide.mapper.ExpMapper;
import com.logistics.micro.pay.provide.model.pojo.ExPojo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * 示例服务实现类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-3-23 22:33
 */
@org.apache.dubbo.config.annotation.Service
@Slf4j
public class ExpServiceImpl extends ServiceImpl<ExpMapper, ExPojo> implements ExpPayApiService {

    @Override
    public ImmutablePair demo() {
        log.info("{}", "we");
        int x = 1 + 1;
        if (x == 2) {
            return ImmutablePair.of(true, "result");
        } else {
            return ImmutablePair.of(false, "result");
        }
    }

}
