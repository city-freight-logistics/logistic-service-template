package com.logistics.micro.pay.api;

import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-03-24 22:14
 */
public interface ExpPayApiService {
    ImmutablePair demo();
}
