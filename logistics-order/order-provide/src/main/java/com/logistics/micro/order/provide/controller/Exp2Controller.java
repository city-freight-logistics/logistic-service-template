package com.logistics.micro.order.provide.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 示例控制器，用于非rpc调用
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-3-23 22:48
 */
@RestController
@RequestMapping("/order")
public class Exp2Controller {
}
