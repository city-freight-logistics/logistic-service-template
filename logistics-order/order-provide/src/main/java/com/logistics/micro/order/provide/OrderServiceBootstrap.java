package com.logistics.micro.order.provide;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Yue Wu
 * @version 1.0
 * @since 2021-3-23 22:21
 */
@SpringBootApplication
@MapperScan("com.logistics.micro.order.provide.mapper")
public class OrderServiceBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(OrderServiceBootstrap.class, args);
    }
}
