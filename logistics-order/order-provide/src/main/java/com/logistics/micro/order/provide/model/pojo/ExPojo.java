package com.logistics.micro.order.provide.model.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 示例数据库实体类
 *
 * @author Yue Wu
 * @version 1.0
 * @since 2021-3-23 22:32
 */
@TableName("t_courier")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExPojo {
    private Integer courierId;
    private String courierName;
    private String courierPhone;
    private String courierCompany;
    private Integer courierNumber;
}
