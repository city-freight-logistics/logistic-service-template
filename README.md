# logistic-service-template

### 介绍
同城物流信息平台微服务端模板库

### 软件架构
软件架构说明

SpringCould Alibaba

### 规范说明

1.  所有模块都需引入公共logistics-common完成依赖引入
2.  所有结构包名详见logistics-order || logistics-pay两个模块示例
3.  Controller返回类型请使用logistics-common中的ApiResponse<T>作为返回类型
4.  返回类型的header请使用HeaderStatus枚举类中的枚举类型
5.  当新增返回头部类型请在枚举类型中新建返回类型，注意分类区分

### 编码及数据库规约

此处尽可能参考阿里巴巴规范手册
